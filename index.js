/**
 * The last value of quizQuestionAnswers
 * @type number[]
 */
let lastValue = []

/** @type {ProxyHandler<any>} */
const parsedHandler = {
  get(target, key, receiver) {
    const result = Reflect.get(target, key, receiver)
    if (key === 'defaultQuizData') {
      if (!result || typeof result.quizQuestionAnswers !== 'object') {
        return { quizQuestionAnswers: lastValue }
      }
      lastValue = result.quizQuestionAnswers
    } else if (key === 'quizQuestionAnswers') {
      return lastValue
    }

    return result
  },
}

JSON.parse = new Proxy(JSON.parse, {
  apply(target, thisArg, args) {
    const parsed = Reflect.apply(target, thisArg, args)
    return new Proxy(parsed, parsedHandler)
  },
})

module.exports = JSON.parse
