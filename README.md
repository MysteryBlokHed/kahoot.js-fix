# kahoot.js fix

Fixes ~~one~~ two critical bugs in [kahoot.js-updated](https://www.npmjs.com/package/kahoot.js-updated).

## The bugs

### The first

In the file `src/modules/backup.js`, kahoot.js tries to get an array from a property
after parsing a response from the Kahoot API. However, this property doesn't always exist.

```js
this.quiz.quizQuestionAnswers = recover.defaultQuizData.quizQuestionAnswers
//                                      ^^^^^^^^^^^^^^^
//                                    this might not exist
```

The value `recover` is retrieved via `JSON.parse`, so this library simply patches `JSON.parse` using a Proxy.
If `defaultQuizData` doesn't exist, its last existant value is returned.
If there is no last valid value, an empty array is returned.

### The second

In another part of the code, there's an attempt to get the key `quizQuestionAnswers` from a different object.
This key, shockingly, is also missing here!
The last valid value or an empty array is also returned in this case.

## License

This project is licensed under either of

- Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or
  <http://www.apache.org/licenses/LICENSE-2.0>)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or
  <http://opensource.org/licenses/MIT>)

at your option.
